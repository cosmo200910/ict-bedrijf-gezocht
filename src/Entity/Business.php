<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Form\Extension\Core\Type\BaseType;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BusinessRepository")
 */
class Business extends BaseType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250, unique=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $address = 'unknown';

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $city = 'unknown';

    /**
     * @ORM\Column(type="integer", length=250)
     */
    private $kvkNumber = 0;

    /**
     * @ORM\Column(type="integer", length=250)
     */
    private $telephoneNumber = 0;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $btwNumber = 'unknown';

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $claimed;

    /**
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="businesses")
     * @ORM\JoinTable(name="businessess_categories")
     */
     private $categories;

    /**
     * @ORM\OneToOne(targetEntity="User", mappedBy="business")
     */
    private $user;

    /**
     * @ORM\ManyToMany(targetEntity="Offer", mappedBy="businesses")
     */
     private $offers;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $address
     */
    public function setAddress($address): void
    {
        $this->address = $address;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @return mixed
     */
    public function getKvkNumber()
    {
        return $this->kvkNumber;
    }

    /**
     * @param mixed $kvkNumber
     */
    public function setKvkNumber($kvkNumber): void
    {
        $this->kvkNumber = $kvkNumber;
    }

    /**
     * @return mixed
     */
    public function getTelephoneNumber()
    {
        return $this->telephoneNumber;
    }

    /**
     * @param mixed $telephoneNumber
     */
    public function setTelephoneNumber($telephoneNumber): void
    {
        $this->telephoneNumber = $telephoneNumber;
    }

    /**
     * @return mixed
     */
    public function getBtwNumber()
    {
        return $this->btwNumber;
    }

    /**
     * @param mixed $btwNumber
     */
    public function setBtwNumber($btwNumber): void
    {
        $this->btwNumber = $btwNumber;
    }

    /**
     * @return mixed
     */
    public function getClaimed()
    {
        return $this->claimed;
    }

    /**
     * @param mixed $claimed
     */
    public function setClaimed($claimed): void
    {
        $this->claimed = $claimed;
    }

    /**
     * @return mixed
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param mixed $categories
     */
    public function setCategories($categories): void
    {
        $this->categories = $categories;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param mixed $offers
     */
    public function setOffers($offers): void
    {
        $this->offers = $offers;
    }

    public function __construct() {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->offers = new \Doctrine\Common\Collections\ArrayCollection();
    }


}
