<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CategoryRepository")
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=250)
     */
    private $categoryName;

    /**
     * @ORM\OneToMany(targetEntity="Offer", mappedBy="category")
     */
    private $offers;

    /**
     * @ORM\ManyToMany(targetEntity="Business", mappedBy="categories")
     */
    private $businesses;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }

    /**
     * @param mixed $categoryName
     */
    public function setCategoryName($categoryName): void
    {
        $this->categoryName = $categoryName;
    }

    /**
     * @return mixed
     */
    public function getOffers()
    {
        return $this->offers;
    }

    /**
     * @param mixed $offers
     */
    public function setOffers($offers): void
    {
        $this->offers = $offers;
    }

    /**
     * @return mixed
     */
    public function getBusinesses()
    {
        return $this->businesses;
    }

    /**
     * @param mixed $businesses
     */
    public function setBusinesses($businesses): void
    {
        $this->businesses = $businesses;
    }

    public function __construct() {
        $this->offers = new ArrayCollection();
        $this->businesses = new ArrayCollection();
    }


}
