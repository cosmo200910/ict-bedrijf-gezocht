<?php

namespace App\Controller;

use App\Entity\Business;
use App\Entity\User;
use App\Helper\ImageResizer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserController extends Controller
{

    /**
     * @Route("/registeren", name="register")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerAction(Request $request, \Swift_Mailer $mailer, UserPasswordEncoderInterface $passwordEncoder)
    {

        //if user loggedin no new registartion needed
        if ($this->getUser() != null)
            return $this->redirectToRoute("homepage");

        $user = new User();
        $business = new Business();

//        $currentYear = new \DateTime();
//        $currentYear->add(date_interval_create_from_date_string("-1 year"));
//        $lastYear = clone $currentYear;
//        $lastYear = $lastYear->add(date_interval_create_from_date_string('-100 years'));
//
//        $lastYear = $lastYear->format('Y');
//        $currentYear = $currentYear->format('Y');
//
//        $yearList = array();
//        for ($i = $currentYear; $i > $lastYear; $i--) {
//            $yearList[$i] = $i;
//        }

        $form = $this->createFormBuilder($user)
            ->add("businessName", TextType::class, array(
                "label" => "Naam", 'attr' => array('placeholder' => 'naam')))
            ->add("email", EmailType::class, array(
                "label" => "E-mail adres", 'attr' => array('placeholder' => 'E-mailadres')))
            ->add("password", PasswordType::class, array(
                "label" => "Wachtwoord", 'attr' => array('placeholder' => 'Wachtwoord')))
            ->add("password_repeat", PasswordType::class, array(
                "label" => 'Herhaal wachtwoord', "mapped" => false, "required" => false, 'attr' => array('placeholder' => 'Wachtwoord')))
            ->add("register", SubmitType::class, array(
                "label" => "Registeren", 'attr' => array('class' => 'float-right btn btn-success')))
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $userCheck = $this->getDoctrine()->getRepository("App:User")->findBy(["email" => $form->get("email")->getData()]);
            if ($userCheck == null) {

                if ($form->get("password")->getData() == $form->get("password_repeat")->getData() && $form->get("password")->getData() != "") {

                    $password = $passwordEncoder->encodePassword($user, $form->get("password")->getData());
                    $user->setPassword($password);
                    $em = $this->getDoctrine()->getManager();

                    $business->setName($user->getBusinessName());
                    $em->persist($business);
                    $em->flush();

                    $user->setBusiness($business);
                    $em->persist($user);
                    $em->flush();


//                    $message = (new \Swift_Message())
//                        ->setSubject('Registratie watvindjij.online')
//                        ->setFrom("accounts@watvindjij.online", "watvindjij.online")
//                        ->setTo($user->getEmail())
//                        ->setBody(
//                            $this->renderView('users/confirmation.html.twig',
//                                array(
//                                    "user" => $user,
//                                    "userUrl" => $request->getSchemeAndHttpHost() . $this->generateUrl("user_activate", array(
//                                                "userId" => $user->getId(),
//                                                "userHash" => sha1($user->getId() . $user->getSalt()))
//                                        )
//                                )
//                            ), 'text/html'
//                        );
//
//                    $mailer->send($message);

                    return $this->redirectToRoute("register_complete");

                } else {
                    $this->addFlash("danger", "De wachtwoorden komen niet overeen");
                }
            } else {
                $this->addFlash("danger", "Dit e-mail adres is reeds in gebruik.");
            }
        } else {
            $errors = $form->getErrors();
            foreach ($errors as $error) {
                $this->addFlash("danger", $error->getMessage());
            }
        }

        return $this->render("users/register.html.twig", array(
            "form" => $form->createView()
        ));

    }

    /**
     * @Route("/registeren/afgerond", name="register_complete")
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function registerCompleteAction()
    {
        return $this->render("users/register_complete.html.twig");
    }

    /**
     * @Route("/wachtwoord-vergeten", name="user_password_forgotten")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function forgottenAction(Request $request, \Swift_Mailer $mailer, UserPasswordEncoderInterface $passwordEncoder)
    {

        $form = $this->createFormBuilder()
            ->add("email", EmailType::class, ["label" => "E-mail adres"])
            ->add("register", SubmitType::class, ["label" => "Opvragen", 'attr' => ['class' => 'btn btn-success']])->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $user = $this->getDoctrine()->getRepository("App:User")->findOneBy(["email" => $form->get("email")->getData()]);
            if ($user != null) {

                $newPassword = $this->generateRandomString(20);
                $password = $passwordEncoder->encodePassword($user, $newPassword);
                $user->setPassword($password);
                $this->getDoctrine()->getManager()->flush();

                $message = (new \Swift_Message())
                    ->setSubject('Wachtwoord vergeten bij ictbedrijfgezocht.nl')
                    ->setFrom("accounts@ictbedrijfgezocht.nl", "ictbedrijfgezocht.nl")
                    ->setTo($user->getEmail())
                    ->setBody(
                        $this->renderView('users/email/forgotten.html.twig',
                            array(
                                "user" => $user,
                                "password" => $newPassword
                            )
                        ), 'text/html'
                    );

                $mailer->send($message);

            }

            $this->addFlash("success", "We hebben je een e-mail gestuurd met verdere instructies.");


        }

        return $this->render("users/forgotten.html.twig", [
            "form" => $form->createView()
        ]);

    }

    /**
     * @Route("/instellingen", name="user_settings")
     * @Security("has_role('ROLE_USER')")
     *
     * @return Response
     */
    public function settingsAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, ImageResizer $imageResizer)
    {

        $user = $this->getUser();
        $business = $user->getBusiness();

        if ($user == null) {
            return $this->redirectToRoute("admin_user_overview");
        }

//        $currentYear = new \DateTime();
//        $currentYear->add(date_interval_create_from_date_string("-1 year"));
//        $lastYear = clone $currentYear;
//        $lastYear = $lastYear->add(date_interval_create_from_date_string('-100 years'));
//
//        $lastYear = $lastYear->format('Y');
//        $currentYear = $currentYear->format('Y');
//
//        $yearList = array();
//        for ($i = $currentYear; $i > $lastYear; $i--) {
//            $yearList[$i] = $i;
//        }

        //<editor-fold desc="User form">
        $form = $this->createFormBuilder($user)
            ->add("email", EmailType::class, ["label" => "E-mail adres"])
            ->add("password", PasswordType::class, ["label" => "Wachtwoord", "mapped" => false, "required" => false])
            ->add("password_repeat", PasswordType::class, ["label" => "Wachtwoord herhalen", "mapped" => false, "required" => false])
            ->add("save", SubmitType::class, ["label" => "Opslaan", 'attr' => ['class' => 'btn btn-success float-right']]);

        $form = $form->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if ($form->get("password")->getData() != "") {

                if ($form->get("password")->getData() == $form->get("password_repeat")->getData()) {

                    $password = $passwordEncoder->encodePassword($user, $form->get("password")->getData());
                    $user->setPassword($password);
                    $this->addFlash("success", "Wachtwoord is gewijzigd.");

                } else {
                    $this->addFlash("danger", "Wachtwoord is niet gewijzigd, de wachtwoorden kwamen niet overeen.");

                }

            }

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "Account instellingen zijn opgeslagen.");

        }
        //</editor-fold>

        return $this->render("users/settings.html.twig", [
            "form" => $form->createView(),
            "user" => $user
        ]);

    }

    /**
     * @Route("/instellingen/bedrijf", name="business_settings")
     * @Security("has_role('ROLE_USER')")
     *
     * @return Response
     */
    public function businessSettings(Request $request, UserPasswordEncoderInterface $passwordEncoder)
    {

        $user = $this->getUser();
        $business = $user->getBusiness();

        if ($user == null) {
            return $this->redirectToRoute("admin_user_overview");
        }

        $businessForm = $this->createFormBuilder($business)
            ->add("name", TextType::class)
            ->add("save", SubmitType::class, ['label' => 'Opslaan', 'attr' => ['class' => 'btn btn-success']]);

        $businessForm = $businessForm->getForm();

        $businessForm->handleRequest($request);

        if ($businessForm->isSubmitted() && $businessForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "Bedrijfs instellingen zijn opgeslagen.");
        }

        return $this->render("users/settings_business.html.twig", [
            "businessForm" => $businessForm->createView(),
            "user" => $user
        ]);

    }

    /**
     * @Route("/activate/{userId}/{userHash}", name="user_activate")
     * @param int $userId
     * @param string $userHash
     * @return Response
     */
    public function activateAction($userId, $userHash)
    {

        $valid = false;
        $user = $this->getDoctrine()->getManager()->getRepository("App:User")->find($userId);
        if ($user != null) {
            if (sha1($user->getId() . $user->getSalt()) == $userHash) {
                $user->setIsActive(1);
                $valid = true;
                $this->getDoctrine()->getManager()->flush();
            }
        }

        if ($valid)
            $this->addFlash("success", "Je bent geactiveerd, je kunt nu inloggen");
        else
            $this->addFlash("danger", "Activatie mislukt, probeer het later nog eens.");

        return $this->redirectToRoute("login", array(), 302);
    }

    /**
     * @Route("/admin/user", name="admin_user_overview")
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param $request Request
     *
     * @return Response
     */
    public function adminOverviewAction(Request $request)
    {

        $users = $this->getDoctrine()->getRepository("App:User")->findAll();

        return $this->render("users/admin/overview.html.twig", [
            "users" => $users
        ]);

    }

    /**
     * @Route("/admin/users/edit/{userId}", name="admin_user_edit")
     * @Security("has_role('ROLE_ADMIN')")
     *
     * @param $userId int
     *
     * @return Response
     */
    public function adminEditAction(Request $request, $userId)
    {

        $user = $this->getDoctrine()->getRepository("App:User")->find($userId);
        if ($user == null) {
            return $this->redirectToRoute("admin_user_overview");
        }

        $form = $this->createFormBuilder($user)
            ->add("name")
            ->add("email")
            ->add("verified")
            ->add("save", SubmitType::class, ["label" => "Opslaan"])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->getDoctrine()->getManager()->flush();
            $this->addFlash("success", "Gebruiker is opgeslagen");

        }

        return $this->render("users/admin/newedit.html.twig", [
            "form" => $form->createView(),
            "user" => $user
        ]);

    }

    private function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

}