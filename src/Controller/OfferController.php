<?php
/**
 * Created by PhpStorm.
 * User: Cosmo
 * Date: 8-3-2018
 * Time: 11:59
 */

namespace App\Controller;
use App\Entity\Impact;
use App\Entity\Offer;
use App\Entity\Category;
use App\Entity\File;
use App\Entity\Status;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Validator\Constraints\Email;


class OfferController extends Controller
{

    /**
    * @Route("/offer", name="sendOffer")
    */
    public function sendOffer(Request $request, \Swift_Mailer $mailer)
    {

        $offer = new Offer();

        $form = $this->createFormBuilder($offer)
            ->add('name', TextType::class, array(
                'label' => 'Naam*'
            ))
            ->add('telephoneNumber', TextType::class, array(
                'label' => 'Telefoon nummer*'
            ))
            ->add('emailAddress', EmailType::class, array(
                'label' => 'Email adres*',
            ))
            ->add('city', TextType::class, array(
                'label' => 'Woonplaats*'
            ))
            ->add('title', TextType::class, array(
                'label' => 'Titel*'
            ))
            ->add('description', TextareaType::class, array(
                'label' => 'Omschrijving*'
            ))
            ->add('category', EntityType::class, array(
                'class' => Category::class,
                'choice_label' => 'categoryName',
                'label' => 'Categorie*'
            ))
            ->add('impact', EntityType::class, array(
                'class' => Impact::class,
                'choice_label' => 'impactName',
                'label' => 'Hoeveel mensen gaan er gebruik van maken?*'
            ))
            ->add('files', FileType::class, array(
                'required' => false,
                'multiple' => true,
                'data_class' => null,
                'label' => 'Upload eventueel bijlages (denk aan logos, huisstijl, demos, etc.)',
            ))
            ->add('status', EntityType::class, array(
                'class' => Status::class,
                'choice_label' => 'statusName',
                'label' => 'De status van het project.*'
            ))
            ->add('deadline', DateType::class, array(
                'widget' => 'choice',
                'label' => 'Deadline',
                'required' => false
            ))
            ->add('save', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid())
        {
            $entityManager = $this->getDoctrine()->getManager();
            $files = $offer->getFiles();

            $offer->setFiles(null);
            $entityManager->persist($offer);
            $entityManager->flush();

            $uploadedFilesArray = array();
            foreach($files as $file)
            {
                $fileName = $this->generateUniqueFilename() . '.' . $file->guessExtension();
                $filePath = $this->getParameter('uploaded_offer_files') .'/'. $fileName;
                $file->move($this->getParameter('uploaded_offer_files'), $fileName);
                $uploadedFile = new File();
                $uploadedFile->setFileName($fileName);
                $uploadedFile->setFilePath($filePath);
                $uploadedFile->setOffer($offer);
                $entityManager->persist($uploadedFile);
                $entityManager->flush();
                array_push($uploadedFilesArray, $uploadedFile);
            }

            $offer->setFiles($uploadedFilesArray);
            $offer->setCreated(new \DateTime());
            $offer->setCreatedBy(NULL);
            $code = substr(md5(uniqid(mt_rand(), true)) , 0, 8);
            $offer->setConfirmCode($code);
            $offer->setIsActive(false);
            $entityManager->persist($offer);
            $entityManager->flush();

            $this->mailConfirmation($offer->getEmailAddress(), $offer->getName(), $offer->getConfirmCode(), $mailer);
            return $this->redirectToRoute('confirmOffer');
        }


        return $this->render('offer/sendoffer.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    private function mailConfirmation($email, $name, $confirmCode, \Swift_Mailer $mailer)
    {
        $message = (new \Swift_Message('Offerte bevestiging'))
            ->setFrom('cosmovandeluitgaarden@gmail.com')
            ->setTo($email)
            ->setBody(
                $this->renderView(
                    'emails/confirmation.html.twig',
                    array('name' => $name, 'confirmCode' => $confirmCode)
                ),
                'text/html'
            );

        $mailer->send($message);
    }


    /**
     * @Route("/offer/confirm", name="confirmOffer")
     */
    public function confirmOffer(Request $request)
    {
        $defaultData = array('placeholder' => 'placeholder');
        $form = $this->createFormBuilder($defaultData)
            ->add('confirmationCode', TextType::class, array(
                'label' => 'Voer hier je code in.'
            ))
            ->add('send', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $data = $form->getData();
            $repository = $this->getDoctrine()->getRepository(Offer::class);

            $offer = $repository->findOneBy([
                'confirmCode' => $data["confirmationCode"],
            ]);

            if(empty($offer))
            {
                die("Verkeerde confirm code.");
            }
            else
            {
                $offer->setIsActive(true);
                $entityManager->persist($offer);
                $entityManager->flush();

                return $this->render('offer/success.html.twig', array(
                    'form' => $form->createView(),
                ));
            }

        }

        return $this->render('offer/confirmoffer.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

}