<?php

namespace App\Repository;

use App\Entity\Impact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Impact|null find($id, $lockMode = null, $lockVersion = null)
 * @method Impact|null findOneBy(array $criteria, array $orderBy = null)
 * @method Impact[]    findAll()
 * @method Impact[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ImpactRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Impact::class);
    }

    /*
    public function findBySomething($value)
    {
        return $this->createQueryBuilder('i')
            ->where('i.something = :value')->setParameter('value', $value)
            ->orderBy('i.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */
}
